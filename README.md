```
::::::::::: :::::::::  ::::    ::::  :::::::::  :::        
    :+:     :+:    :+: +:+:+: :+:+:+ :+:    :+: :+:        
    +:+     +:+    +:+ +:+ +:+:+ +:+ +:+    +:+ +:+        
    +#+     +#++:++#+  +#+  +:+  +#+ +#++:++#+  +#+        
    +#+     +#+    +#+ +#+       +#+ +#+        +#+        
    #+#     #+#    #+# #+#       #+# #+#        #+#        
    ###     #########  ###       ### ###        ########## 
```
**TBMPL** stands for TimberBorn Mimic Plugin Library or simply PLugins.
It is a library that serves as a support library (base) for all [Timberborn](https://mechanistry.com) mods.

Many things have changed since the version of the game with Update 6, and BepInEx is no longer needed to load mods, but the game loads them by itself. This also changed TBMPL Core, which now serves as a support library (previously also used for loading mods).

### Tutorials, Samples, Links

- [Guide: TBMPL - Mods](https://mod.io/g/timberborn/r/tbmpl-mods)
- [Profile: Mimicz (with all mods)](https://mod.io/g/timberborn/u/mimicz/)

### Mods
If the mod is checked, it works for the current Update of the game for which this branch is.\
A detailed description of each mod with images is on **[mod.io](https://mod.io/g/timberborn?tags-in=Mod)**.

##### **Libraries**
- [x] **[YamlDotNet](https://mod.io/g/timberborn/m/yamldotnet)**
- [x] **[TBMPL Core](https://mod.io/g/timberborn/m/tbmpl-core)** (only requires YamlDotNet)
##### **Mods**
- [x] **[Better Batteries](https://mod.io/g/timberborn/m/better-batteries)**
- [x] **[Better Beaver Names](https://mod.io/g/timberborn/m/better-beaver-names)**
- [x] **[Better Beaver Speed](https://mod.io/g/timberborn/m/better-beaver-speed)**
- [x] **[Better Wind Speed](https://mod.io/g/timberborn/m/better-wind-speed)**
- [x] **[Buildings Boost](https://mod.io/g/timberborn/m/buildings-boost)**
- [x] **[Carrier Changer](https://mod.io/g/timberborn/m/carrier-changer)**
- [x] **[Developer Mode](https://mod.io/g/timberborn/m/developer-mode)**
- [x] **[District Center Public Storage](https://mod.io/g/timberborn/m/district-center-public-storage)**
- [x] **[Endless Pipes](https://mod.io/g/timberborn/m/endless-pipes)**
- [x] **[Faster Growth](https://mod.io/g/timberborn/m/faster-growth)**
- [x] **[Fast Pumps](https://mod.io/g/timberborn/m/fast-pumps)**
- [x] **[Fixed Time of Day](https://mod.io/g/timberborn/m/fixed-time-of-day)**
- [x] **[Flood Population (Progressively Increasing Difficulty)](https://mod.io/g/timberborn/m/flood-population)**
- [x] **[Game Speed Control](https://mod.io/g/timberborn/m/game-speed-control)**
- [x] **[Large Districts](https://mod.io/g/timberborn/m/large-districts-longer-routes)**
- [x] **[Larger Stocks & Tanks](https://mod.io/g/timberborn/m/larger-stocks-tanks)**
- [x] **[Longer Life](https://mod.io/g/timberborn/m/longer-life)**
- [x] **[Map Size Unlocker](https://mod.io/g/timberborn/m/map-size-unlocker)**
- [x] **[More Inhabitants](https://mod.io/g/timberborn/m/more-inhabitants)**
- [x] **[More Visitors](https://mod.io/g/timberborn/m/more-visitors)**
- [x] **[More Workers](https://mod.io/g/timberborn/m/more-workers)**
- [x] **[No Badtide Weather](https://mod.io/g/timberborn/m/no-badtide-weather)**
- [x] **[No Construction Limits](https://mod.io/g/timberborn/m/no-construction-limits)**
- [x] **[No Fog](https://mod.io/g/timberborn/m/no-fog)**
- [x] **[No Stuck / Auto-Unstuck](https://mod.io/g/timberborn/m/no-stuck)**
- [x] **[No Stumps](https://mod.io/g/timberborn/m/no-stumps)**
- [x] **[No Welcome Window](https://mod.io/g/timberborn/m/no-welcome-window)**
- [x] **[Recipes Boost](https://mod.io/g/timberborn/m/recipes-boost)**
- [x] **[Research Boost](https://mod.io/g/timberborn/m/research-boost)**
- [x] **[Status Panel Ex](https://mod.io/g/timberborn/m/extended-status-panel)**
- [x] **[Uncontrolled Reproduction](https://mod.io/g/timberborn/m/uncontrolled-reproduction)**
- [x] **[Water Source Control](https://mod.io/g/timberborn/m/water-source-control)**

### How to make your own mod
These basic things are needed.
- Each mod must have a dependency on TBMPL Core and YamlDotNet for configurations.
- Each mod must now have a manifest.json file that the game uses to load the mod.
- If the mod has its own configuration, it must have a config.yaml file.
- Each mod should have at least 2 .cs files, one for the mod's EndPoint and the other for the configuration. It will be described below.

> An ideal example for inspiration is the mod: **BetterWindSpeed** ​​or the more complex **RecipesBoost** or super simple **NoFog**.

#### Steps
**1)** Create a new DLL library project for example via Visual Studio.\
**2)** Add these basic dependencies: **0Harmony, UnityEngine, UnityEngine.CoreModule, TBMPL Core** (theoretically it is only needed is 0Harmony and TBMPL Core. It depends on what exactly the plugin will do)\
**3)** Create a configuration class (E.g. MyConfig.cs) which will inherit BaseConfig.
**4)** Create a class (E.g. MyMod.cs) that will serve as the entry point for loading the plugin. This class must inherit from `GameMod<MyConfig>`.
**5)** That's all!

#### How to do
**1)** Access my configuration from anywhere?\
Add to MyMod property `public static new MyConfig Config { get; private set; }`. Subsequently, the access will be `MyMod.Config.Something`.

###### This is the **minimal** setup that is sufficient for the mod to load successfully.
```csharp
using TBMPL.Core.GameMod;
using TBMPL.Core.GameMod.Config;

namespace TBMPL.MyModName
{
    internal sealed class EP : GameMod<EPConfig>
    {
        public static new EPConfig Config { get; private set; }
    }

    internal sealed class EPConfig : BaseConfig
    {
        public int AnyValue { get; set; }
    }
}
```
###### Here is a sample of the assembly headers in the project.
```xml
<PropertyGroup>
    <TargetFramework>netstandard2.1</TargetFramework>
    <AllowUnsafeBlocks>True</AllowUnsafeBlocks>
    <Nullable>enable</Nullable>
    <!-- Here you will be the author (you) -->
    <Authors>mimic</Authors>
    <Copyright>mimic</Copyright>
    <!-- The name of the mod -->
    <Description>TimberBorn - TBMPL My Mod Name</Description>
    <Product>TBMPL</Product>
    <!-- The namespace should have the name of the mod without spaces -->
    <AssemblyName>TBMPL.MyModName</AssemblyName>
    <AssemblyDirName>TBMPL.MyModName</AssemblyDirName>
    <RootNamespace>$(AssemblyName.Replace(" ", "_"))</RootNamespace>
    <!--The major version is determined by the Update of the game for which the mod is made -->
    <Version>6.0.0</Version>
    <!-- Path to game libraries -->
    <GameManagedRoot>D:\Games\TimberbornExperimental\Timberborn_Data\Managed</GameManagedRoot>
    <TempModsPath>..\.Mods</TempModsPath>
    <GameModsPath>%userprofile%\Documents\Timberborn\Mods</GameModsPath>
</PropertyGroup>

<ItemGroup>
    <!-- This is what the dependency looks like if Core is part of the project -->
    <Reference Include="0Harmony">
        <HintPath>..\TBMPL_Core\Deps\Harmony\0Harmony.dll</HintPath>
    </Reference>
    <!-- This is what a game library dependency looks like -->
    <Reference Include="Timberborn.BaseComponentSystem">
        <HintPath>$(GameManagedRoot)\Timberborn.BaseComponentSystem.dll</HintPath>
    </Reference>
</ItemGroup>

<!-- Insert files into the project -->
<ItemGroup>
    <None Update="config.yaml">
        <CopyToOutputDirectory>Always</CopyToOutputDirectory>
    </None>
    <None Update="manifest.json">
        <CopyToOutputDirectory>Always</CopyToOutputDirectory>
    </None>
</ItemGroup>

<!-- This should always be the same. If there is no configuration, then remove config.yaml -->
<Target Name="PostBuild" AfterTargets="PostBuildEvent">
    <Exec Command="xcopy &quot;$(TargetDir)$(TargetFileName)&quot; &quot;$(TempModsPath)\$(AssemblyDirName)\Plugins\&quot; /R /I /Y /F" />
    <Exec Command="xcopy &quot;$(ProjectDir)config.yaml&quot; &quot;$(TempModsPath)\$(AssemblyDirName)\Configs\&quot; /R /I /Y /F" />
    <Exec Command="xcopy &quot;$(ProjectDir)manifest.json&quot; &quot;$(TempModsPath)\$(AssemblyDirName)\&quot; /R /I /Y /F" />

    <Exec Command="xcopy &quot;$(TempModsPath)\$(AssemblyDirName)&quot; &quot;$(GameModsPath)\$(AssemblyDirName)\&quot; /E /R /I /Y /F" />
</Target>
```
###### Here is an example when the plugin has some custom settings. There's more stuff here that isn't normally needed, so I've commented everything out in the code.
```csharp
namespace TBMPL.MyModName
{
    internal sealed class EP : GameMod<EPConfig>
    {
        public static new EPConfig Config { get; private set; }
    }

    internal sealed class EPConfig : BaseConfig
    {
        public int AnyValue { get; set; }
    }

    public override void Initialize(IModHolder holder)
    {
        // Custom code after loading the mod
    }

    public override void DependencyContainerInstaller(ContainerDefinitionInstallerService service)
    {
        // Installation of custom configurators in the scene such as Game, Map Editor, Main Menu
        service.Install(SceneType.MainMenu, new MyGameConfigurator()); // example
    }

    public override void DependencyContainerReady(SceneType sceneType, DependencyContainer container)
    {
        // The container is initialized and ready to use. This happens according to the scene - loading the menu, game, editor

        if (sceneType == SceneType.Game)
        {
            // Example. Custom code when the game starts (loads the map)
            ConstructionModeService service = container.GetInstance<ConstructionModeService>();
        }
    }

    public override void EverythingReady()
    {
        // All loaded mods for the game
        IList<ISimpleModHolder> allMods = GameModHolder.GameMods;
        // List of all loaded TBMPL mods
        IList<IGameMod> tbmplMods = GameModHolder.CoreMods;
    }

    // Custom configurator
    internal sealed class MyGameConfigurator : AbstractConfigurator
    {
        protected override void Bindings()
        {
            AsSingleton<MyGameComponent>();
        }
    }

    // Custom component
    internal sealed class MyGameComponent
    {
        public MyGameComponent(IContainer container)
        {
            Log.Debug("Hello from Main Menu");
        }
    }
}
```